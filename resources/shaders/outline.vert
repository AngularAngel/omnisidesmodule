#version 140

uniform mat4 u_projection_matrix; //Supplied once per frame.
uniform mat4 u_view_matrix;
uniform mat4 u_model_matrix;

in vec3 in_pos; //Supplied by the vertex buffer.

out vec4 v_view_pos;

void main()
{

    //Position should be a vec4 because matrix transformations require homogeneous coordinates.
    //So this ends up being vec4(x, y, z, 1.0)
    vec4 pos = vec4(in_pos, 1.0);

    v_view_pos = u_view_matrix*u_model_matrix*pos;
    
    //gl_Position is one of the few built-in variables in GLSL.
    gl_Position = u_projection_matrix*u_view_matrix*u_model_matrix*pos;
}
