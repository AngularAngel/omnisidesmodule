#version 150

layout (triangles) in;
layout (triangle_strip, max_vertices=18) out;

uniform mat4 u_view_matrices[6];

void main()
{
    for(int face = 0; face < 6; face++)
    {
        gl_Layer = face; // built-in variable that specifies to which face we render.
        for(int i = 0; i < 3; ++i) {// for each triangle vertex
            gl_Position = u_view_matrices[face] * gl_in[i].gl_Position;
            EmitVertex();
        }
        EndPrimitive();
    }
}  