
module omni.sides {
    requires java.logging;
    requires static lombok;
    requires omni.registry;
    requires devil.util;
    requires omni.module;
    requires omni.world;
    requires omni.serialization;
    requires org.lwjgl.opengl;
    exports net.angle.omnisides.api;
    exports net.angle.omnisides.impl;
}