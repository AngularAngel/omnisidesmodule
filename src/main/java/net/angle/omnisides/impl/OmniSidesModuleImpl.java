package net.angle.omnisides.impl;

import java.util.logging.Level;
import java.util.logging.Logger;
import net.angle.omnimodule.impl.BasicOmniModule;
import net.angle.omniregistry.api.Registry;
import net.angle.omniregistry.api.RegistryCollection;
import net.angle.omniregistry.impl.BasicDatumRegistry;
import net.angle.omniserialization.api.ObjectSerializerRegistry;
import net.angle.omniserialization.impl.ReflectiveSerializer;
import net.angle.omnisides.api.OmniSidesModule;
import net.angle.omnisides.api.Side;
import net.angle.omniworld.api.space.BlockFace;
import net.angle.omniworld.api.chunks.VoxelComponent;
import net.angle.omniworld.impl.chunks.BasicDatumVoxelComponent;

/**
 *
 * @author angle
 */
public class OmniSidesModuleImpl extends BasicOmniModule implements OmniSidesModule {

    public OmniSidesModuleImpl() {
        super(OmniSidesModule.class);
    }
    
    @Override
    public void populateRegistries(RegistryCollection registries, BlockFace[] blockFaces) {
        Registry<Side> sidesRegistry = (Registry<Side>) registries.addEntry(new BasicDatumRegistry<>("Sides", registries));
        
        Registry<VoxelComponent> chunkComponents = (Registry<VoxelComponent>) registries.getEntryByName("Chunk Components");
        
        
        for (BlockFace blockFace : blockFaces) {
            chunkComponents.addEntry(new BasicDatumVoxelComponent<>(blockFace.getName() + " Sides", chunkComponents, Side.class, sidesRegistry));
        }
    }
    
    @Override
    public void prepSerializerRegistry(ObjectSerializerRegistry serializerRegistry) {
        try {
            serializerRegistry.registerObject(BasicSide.class, new ReflectiveSerializer<>(BasicSide.class.getConstructor(String.class, int.class, int.class, int.class, boolean.class, boolean.class), (t) -> {
                return new Object[]{t.getName(), t.getRegistryID(), t.getId(), t.getTextureDefinitionID(), t.isTransparent(), t.isDrawable()};
            }));
        } catch (NoSuchMethodException | SecurityException ex) {
            Logger.getLogger(OmniSidesModuleImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}