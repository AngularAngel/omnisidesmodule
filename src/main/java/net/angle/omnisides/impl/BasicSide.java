/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.angle.omnisides.impl;

import lombok.EqualsAndHashCode;
import lombok.ToString;
import net.angle.omniregistry.api.Registry;
import net.angle.omniregistry.impl.AbstractRenderable;
import net.angle.omnisides.api.Side;

/**
 *
 * @author angle
 * @license https://gitlab.com/AngularAngel/omnicraft/-/blob/master/LICENSE
 */
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class BasicSide extends AbstractRenderable implements Side {
    
    public BasicSide(String name, Registry registry, int textureDefinitionID, boolean transparent, boolean drawable) {
        super(name, registry, textureDefinitionID, transparent, drawable);
    }
    
    public BasicSide(String name, int registryID, int id, int textureDefinitionID) {
        super(name, registryID, id, textureDefinitionID, false, true);
    }
    
    public BasicSide(String name, int registryID, int id, int textureDefinitionID, boolean transparent, boolean drawable) {
        super(name, registryID, id, textureDefinitionID, transparent, drawable);
    }
    
    public BasicSide(String name, Registry registry, int textureDefinitionID) {
        super(name, registry, textureDefinitionID, false, true);
    }
    
    @Override
    public String toString() {
        return "BasicSide(" + getName() + ", TextureID: " + getTextureDefinitionID() + ", Drawable: " + isDrawable() + ", Transparent: " + isTransparent() + ")";
    }
}