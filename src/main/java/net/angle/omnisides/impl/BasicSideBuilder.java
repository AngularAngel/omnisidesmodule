package net.angle.omnisides.impl;

import net.angle.omniregistry.impl.AbstractRenderableBuilder;

/**
 *
 * @author angle
 */
public class BasicSideBuilder extends AbstractRenderableBuilder<BasicSide> {
    public BasicSideBuilder() {
        super(BasicSide.class);
    }
    
    @Override
    public BasicSide build() {
        checkBuildability();
        return build(new BasicSide(getName(), getRegistry(), getTextureDefinitionID(), isTransparent(), isDrawable()));
    }
}