package net.angle.omnisides.api;

import net.angle.omnimodule.api.OmniModule;
import net.angle.omniregistry.api.RegistryCollection;
import net.angle.omniserialization.api.ObjectSerializerRegistry;
import net.angle.omniworld.api.space.BlockFace;
import org.lwjgl.opengl.GL13C;

/**
 *
 * @author angle
 */
public interface OmniSidesModule extends OmniModule {
    public void populateRegistries(RegistryCollection registries, BlockFace[] blockFaces);
    public void prepSerializerRegistry(ObjectSerializerRegistry serializerRegistry);
}