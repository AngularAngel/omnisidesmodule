package net.angle.omnisides.api;

import net.angle.omniregistry.api.Renderable;

/**
 *
 * @author angle
 */
public interface Side extends Renderable {
    

    @Override
    public default boolean isTransparent() {
        return false;
    }

    @Override
    public default boolean isDrawable() {
        return true;
    }
}